//
// Created by serhii on 18.04.20.
//

#ifndef SPELLER_TRIEDICTIONARY_H
#define SPELLER_TRIEDICTIONARY_H


#include <string>
#include "trie_tree/raw_ptr_impl/TrieTree.h"
#include "Dictionary.h"

class TrieDictionary : public Dictionary {
private:
    TrieTree _words;
public:
    TrieDictionary();

    virtual bool load(const std::string& dictionaryFileName) override;
    virtual bool check(std::string word) override;
    virtual void display() override;
};


#endif //SPELLER_TRIEDICTIONARY_H
