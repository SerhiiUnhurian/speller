#include "HashTable.h"

// hash(s) = (s[0] + s[1]⋅p + s[2]⋅p^2 +...+ s[n−1]⋅p^(n−1)) % m
int HashTable::hashFunc(const std::string &data) const {
    // p is some chosen prime number for reducing collisions.
    const int p {31};
    long long hashValue = 0;
    long long pPower = 1; // p^0 = 1;

    for (char c : data) {
        hashValue = (hashValue + c * pPower) % _slots;
        pPower = (pPower * p) % _slots;
    }

    return (int)hashValue;
}

void HashTable::insert(const std::string &data) {
    int hashValue = hashFunc(data);
    auto& slot = _table[hashValue];
    auto it = slot.begin();

    for (; it != slot.end(); it++) {
        const std::string& tmp = *it;

        if (data == tmp) {
            return;
        } else if (data < tmp) {
            slot.insert(it, data);
            _size++;
            return;
        }
    }
    slot.insert(it, data);
    _size++;
}

void HashTable::remove(const std::string &data) {
    int hashValue = hashFunc(data);
    auto& slot = _table[hashValue];
    auto it = slot.begin();

    for (; it != slot.end(); it++) {
        const std::string& tmp = *it;

        if (data < tmp) {
            return;
        } else if (data == tmp) {
            it = slot.erase(it);
            _size--;
            return;
        }
    }
}

bool HashTable::check(const std::string &data) const {
    int hashValue = hashFunc(data);
    auto& slot = _table[hashValue];
    auto it = slot.begin();

    for (; it != slot.end(); it++) {
        const std::string& tmp = *it;

        if (data < tmp) {
            return false;
        } else if (data == tmp) {
            return true;
        }
    }
    return false;
}

void HashTable::display() {
    for (const auto& slot : _table) {
        if (slot.empty()) continue;

        for (const auto& str : slot) {
            std::cout << str << std::endl;
        }
    }
}

int HashTable::size() const {
    return _size;
}

bool HashTable::isEmpty() const {
    return _size == 0;
}

int HashTable::maxCollisions() const {
    int maxCollisions {0};

    for (const auto& slot : _table) {
        if (slot.empty()) continue;

        if (slot.size() > maxCollisions) {
            maxCollisions = slot.size();
        }
    }
    return maxCollisions - 1;
}
