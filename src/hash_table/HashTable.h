#ifndef SPELLER_HASHTABLE_H
#define SPELLER_HASHTABLE_H

#include <array>
#include <list>
#include <iostream>

// Use prime number for reducing collisions.
constexpr int MAX_SLOTS = 20'011;

class HashTable {
private:
    int _size {};
    static const int _slots {MAX_SLOTS};
    std::array<std::list<std::string>, _slots> _table {};

    int hashFunc(const std::string& data) const;
public:
    int size() const;
    bool isEmpty() const;
    int maxCollisions() const;
    void insert(const std::string& data);
    void remove(const std::string& data);
    bool check(const std::string& data) const;
    void display();
};

#endif //SPELLER_HASHTABLE_H
