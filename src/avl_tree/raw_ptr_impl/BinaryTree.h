/*
 * Binary search tree implemented as self-balancing AVL tree.
 */

#ifndef SPELLER_BINARYTREE_H
#define SPELLER_BINARYTREE_H

#include <cstring>
#include <sstream>
#include <memory>
#include <iostream>

#include "Node.h"

template <class T>
class BinaryTree {
private:
    Node<T>* _root {nullptr};

    // Deletes data from the tree if there is such data.
    Node<T>* del(T data, Node<T>* currentNode) {
        if (currentNode == nullptr) {
            return nullptr;
        } else if (data < currentNode->_data) {
            currentNode->_left = del(data, currentNode->_left);
        } else if (data > currentNode->_data) {
            currentNode->_right = del(data, currentNode->_right);
        } else if (currentNode->_left == nullptr && currentNode->_right == nullptr) { // if data == currentNode->data && currentNode is leaf
            delete currentNode;
            return nullptr;
        } else { // if data == currentNode->data && currentNode is not leaf
            int rightNodeHeight = currentNode->_right->_height;
            int leftNodeHeight = currentNode->_left->_height;
            Node<T>* node {nullptr};

            // Decides which subtree substitution node will be taken from.
            if ( rightNodeHeight > leftNodeHeight ) {
                node = minChild(currentNode->_right);
                currentNode->_data = node->_data;
                currentNode->_right = del(node->_data, currentNode->_right);
            } else {
                node = maxChild(currentNode->_left);
                currentNode->_data = node->_data;
                currentNode->_left = del(node->_data, currentNode->_left);
            }
        }

        // Update the current node height.
        currentNode->updateHeight();

        // If current node is imbalanced, perform necessary rotation.
        if (balanceFactor(currentNode) == 2 && (balanceFactor(currentNode->_left) == 1 || balanceFactor(currentNode->_left) == 0)) {
            return  LL_rotate(currentNode);
        } else if (balanceFactor(currentNode) == 2 && balanceFactor(currentNode->_left) == -1) {
            return LR_rotate(currentNode);
        } else if (balanceFactor(currentNode) == -2 && (balanceFactor(currentNode->_right) == -1 || balanceFactor(currentNode->_right) == 0)) {
            return RR_rotate(currentNode);
        } else if (balanceFactor(currentNode) == -2 && balanceFactor(currentNode->_right) == 1) {
            return RL_rotate(currentNode);
        }

        return currentNode;
    }

    // Add data to the tree if there is no such data yet.
    Node<T>* add(T data, Node<T>* currentNode) {
        if (!currentNode) {
            return new Node<T>(data);
        } else if (data < currentNode->_data) {
            currentNode->_left = add(data, currentNode->_left);
        } else if (data > currentNode->_data) {
            currentNode->_right = add(data, currentNode->_right);
        }

        // Update the current node height after node insertion.
        currentNode->updateHeight();

        // If current node is imbalanced, perform necessary rotation.
        if (balanceFactor(currentNode) == 2 && balanceFactor(currentNode->_left) == 1) {
            return  LL_rotate(currentNode);
        } else if (balanceFactor(currentNode) == 2 && balanceFactor(currentNode->_left) == -1) {
            return LR_rotate(currentNode);
        } else if (balanceFactor(currentNode) == -2 && balanceFactor(currentNode->_right) == -1) {
            return RR_rotate(currentNode);
        } else if (balanceFactor(currentNode) == -2 && balanceFactor(currentNode->_right) == 1) {
            return RL_rotate(currentNode);
        }

        return currentNode;
    }

    // Checks if the data is present in the tree.
    bool check(T data, Node<T>* node) {
        if (!node) {
            return false;
        }
        else if (data == node->_data) {
            return true;
        }
        else if (data < node->_data) {
            return check(data, node->_left);
        }
        else if (data > node->_data) {
            return check(data, node->_right);
        }
    }

    // Perform single left rotation on the given node.
    Node<T>* LL_rotate(Node<T>* node) {
        Node<T> *lChild = node->_left; // left child of the node.
        Node<T> *lrChild = lChild->_right; // right child of the left child.

        lChild->_right = node;
        node->_left = lrChild;

        node->updateHeight();
        lChild->updateHeight();

        return lChild;
    }

    // Perform single right rotation on the given node.
    Node<T>* RR_rotate(Node<T>* node) {
        Node<T> *rChild = node->_right; // right child of the node.
        Node<T> *rlChild = rChild->_left; // left child of the right node.

        rChild->_left = node;
        node->_right = rlChild;

        node->updateHeight();
        rChild->updateHeight();

        return rChild;
    }

    // Perform left-right rotation on the given node.
    Node<T>* LR_rotate(Node<T>* node) {
        Node<T> *lChild = node->_left;
        Node<T> *lrChild = lChild->_right;

        lChild->_right = lrChild->_left;
        node->_left = lrChild->_right;

        lrChild->_left = lChild;
        lrChild->_right = node;

        node->updateHeight();
        lChild->updateHeight();
        lrChild->updateHeight();

        return lrChild;
    }

    // Perform right-left rotation on the given node.
    Node<T>* RL_rotate(Node<T>* node) {
        Node<T> *rChild = node->_right;
        Node<T> *rlChild = rChild->_left;

        rChild->_left = rlChild->_right;
        node->_right = rlChild->_left;

        rlChild->_left = node;
        rlChild->_right = rChild;

        node->updateHeight();
        rChild->updateHeight();
        rlChild->updateHeight();

        return rlChild;
    }

    // Finds the child with minimum value.
    Node<T>* minChild(Node<T>* node) {
        while (node->_left != nullptr) {
            node = node->_left;
        }
        return node;
    }

    // Finds the child with maximum value.
    Node<T>* maxChild(Node<T>* node) {
        while (node->_right != nullptr) {
            node = node->_right;
        }
        return node;
    }

    // Calculates balance factor of the given node.
    int balanceFactor(Node<T>* node) {
        int leftChildHeight = (node->_left) ? node->_left->_height : 0;
        int rightChildHeight = (node->_right) ? node->_right->_height : 0;

        return leftChildHeight - rightChildHeight;
    }

    // Prints nodes of the tree inorder.
    void inorder(Node<T>* node) {
        if (!node) {
            return;
        }
        inorder(node->_left);
        std::cout << node->_data << " ";
        inorder(node->_right);
    }

public:
    void add(T data) {
        _root = add(data, _root);
    }

    void erase(T data) {
        _root = del(data, _root);
    }

    bool check(T data) {
        return  check(data, _root);
    }

    void display() {
        inorder(_root);
    }
};

#endif // SPELLER_BINARYTREE_H
