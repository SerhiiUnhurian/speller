#ifndef BINARY_TREE_NODE_H
#define BINARY_TREE_NODE_H

#include <memory>

template <class T>
class Node {
public:
    T _data;
    Node<T>* _left {nullptr};
    Node<T>* _right {nullptr};
    int _height {1};

    Node(T data) : _data(data) {}

    void updateHeight() {
        int leftChildHeight = (_left) ? _left->_height : 0;
        int rightChildHeight = (_right) ? _right->_height : 0;

        _height = (leftChildHeight > rightChildHeight) ? (leftChildHeight + 1) : (rightChildHeight + 1);
    }
};

#endif //BINARY_TREE_NODE_H
