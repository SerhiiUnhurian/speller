#ifndef BINARY_TREE_NODE_H
#define BINARY_TREE_NODE_H

#include <memory>

template <class T>
class Node_unique {
public:
    T _data;
    std::unique_ptr<Node_unique<T>> _left {nullptr};
    std::unique_ptr<Node_unique<T>> _right {nullptr};

    Node_unique(T data) : _data(data) {}
};

#endif //BINARY_TREE_NODE_H
