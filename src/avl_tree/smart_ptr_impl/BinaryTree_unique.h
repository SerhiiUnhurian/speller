/*
 * Binary search tree implemented as self-balancing AVL tree using smart pointers.
 */

#ifndef SPELLER_BINARYTREE_H
#define SPELLER_BINARYTREE_H

#include <cstring>
#include <sstream>
#include <memory>
#include <iostream>

#include "Node_unique.h"

template <class T>
class BinaryTree_unique {
private:
    std::unique_ptr<Node_unique<T>> _root {nullptr};

    void add(T data, std::unique_ptr<Node_unique<T>>& node) {

        if (!node) {
            node = std::make_unique<Node_unique<T>>(data);
        }
        else if (data == node->_data) {
            return;
        }
        else if (data < node->_data) {
            add(data, node->_left);
        }
        else if (data > node->_data) {
            add(data, node->_right);
        }
    }

    bool check(T data, std::unique_ptr<Node_unique<T>>& node) {
        if (!node) {
            return false;
        }
        else if (data == node->_data) {
            return true;
        }
        else if (data < node->_data) {
            return check(data, node->_left);
        }
        else if (data > node->_data) {
            return check(data, node->_right);
        }
    }

    void inorder(std::unique_ptr<Node_unique<T>>& node) {
        if (!node) {
            return;
        }
        inorder(node->_left);
        std::cout << node->_data << " ";
        inorder(node->_right);
    }

    void parseStr(const std::string &input) {
        std::stringstream ss;
        ss << input;

        std::string tmp;
        T data;

        while (!ss.eof()) {
            ss >> tmp;
            if (std::stringstream(tmp) >> data) {
                add(data);
            }
        }
    }

public:
    BinaryTree_unique() = default;

    BinaryTree_unique(const std::string& input) : BinaryTree_unique() {
        parseStr(input);
    }

    void add(T data) {
        add(data, _root);
    }

    bool check(T data) {
        return  check(data, _root);
    }

    void display() {
        inorder(_root);
    }
};

#endif // SPELLER_BINARYTREE_H
