//
// Created by serhii on 23.04.20.
//

#ifndef SPELLER_HASHTABLEDICTIONARY_H
#define SPELLER_HASHTABLEDICTIONARY_H


#include "Dictionary.h"
#include "hash_table/HashTable.h"

class HashTableDictionary : public Dictionary {
private:
    HashTable _words;
public:
    HashTableDictionary();

    virtual bool load(const std::string& dictionaryFileName) override;
    virtual bool check(std::string word) override;
    virtual void display() override;
};


#endif //SPELLER_HASHTABLEDICTIONARY_H
