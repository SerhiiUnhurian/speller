//
// Created by serhii on 18.04.20.
//

#include <fstream>
#include "TrieDictionary.h"

TrieDictionary::TrieDictionary() : Dictionary("TrieDictionary") { }

bool TrieDictionary::load(const std::string &dictionaryFileName) {
    std::string word;
    std::ifstream dictionaryFile(dictionaryFileName);

    if (!dictionaryFile) {
        return false;
    }

    dictionaryFile >> word;

    while (dictionaryFile) {
        _words.add(word);
        _size++;
        dictionaryFile >> word;
    }

    dictionaryFile.close();

    return true;
}

bool TrieDictionary::check(std::string word) {
    std::transform(word.begin(), word.end(), word.begin(),
                   [](unsigned char c) {
                       return std::tolower(c);
                   });


    return _words.check(word) != nullptr;
}

void TrieDictionary::display() {

}
