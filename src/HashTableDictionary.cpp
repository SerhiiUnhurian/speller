//
// Created by serhii on 23.04.20.
//

#include <fstream>
#include "HashTableDictionary.h"

HashTableDictionary::HashTableDictionary() : Dictionary("HashTableDictionary") { }

bool HashTableDictionary::load(const std::string &dictionaryFileName) {
    std::string word;
    std::ifstream dictionaryFile(dictionaryFileName);

    if (!dictionaryFile) {
        return false;
    }

    dictionaryFile >> word;

    while (dictionaryFile) {
        _words.insert(word);
        _size++;
        dictionaryFile >> word;
    }

    dictionaryFile.close();

    return true;
}

bool HashTableDictionary::check(std::string word) {
    std::transform(word.begin(), word.end(), word.begin(),
                   [](unsigned char c) {
                       return std::tolower(c);
                   });

    return _words.check(word);
}

void HashTableDictionary::display() {
    _words.display();
}