//
// Created by serhii on 17.04.20.
//

#ifndef SPELLER_SETDICTIONARY_H
#define SPELLER_SETDICTIONARY_H


#include <string>
#include <unordered_set>
#include "Dictionary.h"

class SetDictionary : public Dictionary {
private:
    std::unordered_set<std::string> _words;
public:
    SetDictionary();

    virtual bool load(const std::string& dictionaryFileName) override;
    virtual bool check(std::string word) override;
    virtual void display() override;
};


#endif //SPELLER_SETDICTIONARY_H
