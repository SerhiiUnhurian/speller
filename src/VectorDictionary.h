//
// Created by serhii on 10.04.20.
//

#ifndef SPELLER_VECTORDICTIONARY_H
#define SPELLER_VECTORDICTIONARY_H

#include <vector>
#include "Dictionary.h"

class VectorDictionary : public Dictionary {
private:
    std::vector<std::string> _words;
public:
    VectorDictionary();

    virtual bool load(const std::string& dictionaryFileName) override;
    virtual bool check(std::string word) override;
    virtual void display() override;
};


#endif //SPELLER_VECTORDICTIONARY_H
