#include "Node.h"

TrieNode::~TrieNode() {
    for (auto child : _children) {
        delete child;
    }
}
