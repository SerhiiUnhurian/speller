#ifndef SPELLER_TRIETREE_H
#define SPELLER_TRIETREE_H

#include <string>

#include "Node.h"
#include <vector>

class TrieTree {
private:
    TrieNode* _root {nullptr};

    void print(const TrieNode* currentNode, std::vector<char>& word);
public:
    TrieTree();
    ~TrieTree();

    // Adds word to the tree.
    void add(const std::string& word);

    // Check if word exists in the tree. If it is, returns pointer to the last node, null - otherwise.
    TrieNode* check(const std::string& word);

    // Deletes word from the tree if it exists in the tree.
    bool del(const std::string& word);

    // Prints all words in the tree in lexicographical order.
    void print();
};


#endif //SPELLER_TRIETREE_H
