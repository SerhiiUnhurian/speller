#ifndef SPELLER_NODE_H
#define SPELLER_NODE_H

constexpr int ALPHABET_SIZE = 27;

class TrieNode {
public:
    ~TrieNode();

    TrieNode* _parent {nullptr};
    TrieNode* _children[ALPHABET_SIZE] {};
    bool isWord {false};
};


#endif //SPELLER_NODE_H
