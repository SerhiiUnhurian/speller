#include <vector>
#include <iostream>
#include "TrieTree.h"
#include "../trie_tree/TrieTree.h"


constexpr int CASE = 97;
constexpr int APOSTROPHE_POS = 26;

void TrieTree::add(const std::string& word) {
    std::shared_ptr<Node> node { _root };

    // Allows only alphabetical characters and apostrophe.
    for (const auto ch : word) {
        if (ch == '\'') {
            // Creates new node, if ch is apostrophe and there isn't corresponding node in the tree.
            if (node->_children[APOSTROPHE_POS] == nullptr) {
                node->_children[APOSTROPHE_POS] = std::make_shared<Node>();
                node->_children[APOSTROPHE_POS]->_parent = node;
            }
            // Moves on to the next node, if ch is apostrophe and there is corresponding node in the tree.
            node = node->_children[APOSTROPHE_POS];
            continue;
        } else if (node->_children[ch - CASE] == nullptr) {
            // Creates new node, if ch is letter and there isn't corresponding node in the tree.
            node->_children[ch - CASE] = std::make_shared<Node>();
            node->_children[ch - CASE]->_parent = node;
        }
        // Moves on to the next node, if ch is letter and there is corresponding node in the tree.
        node = node->_children[ch - CASE];
    }
    node->isWord = true;
}

std::shared_ptr<Node> TrieTree::check(const std::string& word) {
    std::shared_ptr<Node> node { _root };

    // Traverses word down the tree. Returns last node of the word if it exists, nullptr - otherwise.
    for (const auto ch : word) {
        if (ch == '\'') {
            if (node->_children[APOSTROPHE_POS] != nullptr)
                node = node->_children[APOSTROPHE_POS];
            else
                return nullptr;
        } else if (node->_children[ch - CASE] != nullptr) // If ch is letter
            node = node->_children[ch - CASE];
        else
            return nullptr;
    }

    return (node->isWord) ? node : nullptr;
}

bool TrieTree::del(const std::string &word) {
    std::shared_ptr<Node> currentNode = check(word);

    // If word exists in the tree.
    if (currentNode != nullptr) {
        currentNode->isWord = false;
        std::shared_ptr<Node> parent {};
        bool isLeaf = true;

        // Traverse through every child in current node to see if the current node is leaf node or it is a part of another word.
        for (const auto& child : currentNode->_children) {
            if (child != nullptr) {
                isLeaf = false;
                break;
            }
        }

        parent = currentNode->_parent;
        // If it's leaf node, traverse word up the tree until the root node or until another word.
        while (isLeaf && parent != nullptr && !currentNode->isWord) {
            // Traverse parent's children, deletes current node and determines if parent node is leaf or not.
            for (auto& child : parent->_children) {
                if (child == currentNode) {
                    child = nullptr;
                    currentNode = parent;
                } else if (child != nullptr) {
                    isLeaf = false;
                    break;
                }
            }
            parent = currentNode->_parent;
        }
        return true;
    }

    return false;
}

void TrieTree::print(const std::shared_ptr<Node>& currentNode, std::vector<char> &word) {
    if (currentNode->isWord) {
        for (const auto &ch : word) {
            std::cout << ch;
        }
        std::cout << std::endl;
    }

    for (int i = 0; i < ALPHABET_SIZE; i++) {
        if (currentNode->_children[i] != nullptr) {
            if (i == APOSTROPHE_POS) {
                word.push_back('\'');
            } else {
                word.push_back(i + CASE);
            }
            print(currentNode->_children[i], word);
            word.pop_back();
        }
    }
}

void TrieTree::print() {
    std::vector<char> word;
    print(_root, word);
}