#ifndef SPELLER_NODE_UNIQUE_H
#define SPELLER_NODE_UNIQUE_H

#include <memory>

constexpr int ALPHABET_SIZE = 27;

class Node {
public:
    std::shared_ptr<Node> _parent {nullptr};
    std::array<std::shared_ptr<Node>, ALPHABET_SIZE> _children { };
    bool isWord {false};
};


#endif //SPELLER_NODE_UNIQUE_H
