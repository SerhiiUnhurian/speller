#ifndef SPELLER_TRIETREE_UNIQUE_H
#define SPELLER_TRIETREE_UNIQUE_H

#include <vector>
#include <string>

#include "Node.h"

class TrieTree {
private:
    std::shared_ptr<Node> _root {std::make_unique<Node>()};

    void print(const std::shared_ptr<Node>& currentNode, std::vector<char>& word);
public:
    // Add word to the tree.
    void add(const std::string& word);
    // Check if word exists in the tree. If it is, returns pointer to the last node, null - otherwise.
    std::shared_ptr<Node> check(const std::string& word);
    // Deletes word from the tree if it exists in the tree.
    bool del(const std::string& word);
    // Prints all words in the tree in lexicographical order.
    void print();
};


#endif //SPELLER_TRIETREE_UNIQUE_H
