//
// Created by serhii on 16.04.20.
//

#ifndef SPELLER_BINARYTREEDICTIONARY_H
#define SPELLER_BINARYTREEDICTIONARY_H

#include <string>

#include "Dictionary.h"
#include "avl_tree/raw_ptr_impl/BinaryTree.h"

class BinaryDictionary : public Dictionary {
private:
    BinaryTree<std::string> _words;
public:
    BinaryDictionary();

    virtual bool load(const std::string& dictionaryFileName) override;
    virtual bool check(std::string word) override;
    virtual void display() override;
};


#endif //SPELLER_BINARYTREEDICTIONARY_H
