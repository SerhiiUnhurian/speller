//
// Created by serhii on 10.04.20.
//

#include <fstream>
#include <iostream>

#include "VectorDictionary.h"

VectorDictionary::VectorDictionary() : Dictionary("StdVector") { }

bool VectorDictionary::load(const std::string& dictionaryFileName) {
    std::string word;
    std::ifstream dictionaryFile(dictionaryFileName);

    if (!dictionaryFile) {
        return false;
    }

    dictionaryFile >> word;

    while (dictionaryFile) {
        _words.push_back(std::move(word));
        _size++;
        dictionaryFile >> word;
    }

    dictionaryFile.close();

    return true;
}

bool VectorDictionary::check(std::string word) {
    std::transform(word.begin(), word.end(), word.begin(),
                   [](unsigned char c) {
                       return std::tolower(c);
                   });

    return std::binary_search(_words.begin(), _words.end(), word);                     ;
}

void VectorDictionary::display() {
    for (const auto& word : _words) {
        std::cout << word << std::endl;
    }
}

//VectorDictionary::VectorDictionary() : Dictionary() {}
