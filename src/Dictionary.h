//
// Created by serhii on 10.04.20.
//

#ifndef SPELLER_DICTIONARY_H
#define SPELLER_DICTIONARY_H

#include <string>
#include <sstream>

class Dictionary {
protected:
    std::string _name;
    std::uint32_t _size {0};
public:
    Dictionary(const std::string &name);
    virtual ~Dictionary();

    const std::string &getName() const;
    std::uint32_t getSize();

    virtual bool load(const std::string& dictionaryName) = 0;
    virtual bool check(std::string word) = 0;
    virtual void display() = 0;

};


#endif //SPELLER_DICTIONARY_H
