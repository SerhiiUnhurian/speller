//
// Created by serhii on 17.04.20.
//

#include <fstream>
#include <algorithm>
#include <iostream>
#include "SetDictionary.h"

SetDictionary::SetDictionary() : Dictionary("SetDictionary") { }

bool SetDictionary::load(const std::string &dictionaryFileName) {
    std::string word;
    std::ifstream dictionaryFile(dictionaryFileName);

    if (!dictionaryFile) {
        return false;
    }

    dictionaryFile >> word;

    while (dictionaryFile) {
        _words.insert(std::move(word));
        _size++;
        dictionaryFile >> word;
    }

    dictionaryFile.close();

    return true;
}

bool SetDictionary::check(std::string word) {
    std::transform(word.begin(), word.end(), word.begin(),
                   [](unsigned char c) {
                       return std::tolower(c);
                   });

    return _words.contains(word);
}

void SetDictionary::display() {
    for (auto it = _words.begin(); it != _words.end(); it++)
        std::cout << *it << std::endl;
//    for (const auto& w : _words)
//        std::cout << w << std::endl;
}
