//
// Created by serhii on 16.04.20.
//

#include <fstream>
#include <algorithm>

#include "BinaryDictionary.h"

BinaryDictionary::BinaryDictionary() : Dictionary("BinaryDictionary") { }

bool BinaryDictionary::load(const std::string &dictionaryFileName) {
    std::string word;
    std::ifstream dictionaryFile(dictionaryFileName);

    if (!dictionaryFile) {
        return false;
    }

    dictionaryFile >> word;

    while (dictionaryFile) {
        _words.add(word);
        _size++;
        dictionaryFile >> word;
    }

    dictionaryFile.close();

    return true;
}

bool BinaryDictionary::check(std::string word) {
    std::transform(word.begin(), word.end(), word.begin(),
                   [](unsigned char c) {
                       return std::tolower(c);
                   });

    return _words.check(word);
}

void BinaryDictionary::display() {
    _words.display();
}
