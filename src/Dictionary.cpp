//
// Created by serhii on 10.04.20.
//

#include <fstream>
#include "Dictionary.h"

Dictionary::Dictionary(const std::string &name) : _name(name) {}

Dictionary::~Dictionary() {}

std::uint32_t Dictionary::getSize() {
    return _size;
}

const std::string &Dictionary::getName() const {
    return _name;
}
