## Проект Speller

1. Реализовать сбалансированное по высоте AVL-дерево.
2. Реализовать сбалансированное по высоте AVL-дерево используя смарт указаккатели.
3. Реализовать структуру данных Trie tree для хранения строк.
4. Реализовать структуру данных Trie tree для хранения строк используя смарт указаккатели.
5. Реализовать Hash Table для хранения строк. Хеш таблица должна выводить число "коллизий" - максимальное количество значений, попавших в один карман.

1. Программа читает один словарь — текстовый файл, в котором находятся слова английского языка.

   * Словарь отсортирован по алфавиту

   * На каждой строке находится только одно слово

   * В словаре может быть до 200 000 слов

   * В словаре могут быть только слова латиницей, все буквы маленькие. Но слово может включать символ ‘ (апостроф)

2. Программа читает из папки тексты, которые по-очереди проверяет на «правильность». Для каждого текста формируется файл со списком «неправильных» слов — то есть тех, которых нет в словаре.

   * Тексты — это реальные тексты книг. Там в строке может быть много слов, есть знаки препинания и пробелы, слова могут быть с заглавными буквами (могут даже быть все заглавные). Перед проверкой программа должна разбивать текст на слова, переводить всё в нижний регистр, выбрасывать знаки препинания и неизвестные символы — допустимыми считаются только маленькие английские буквы и апостроф

   * Хранение словаря и проверку нужно реализовать на базе нескольких структур данных: встроенного типа "vector", встроенного типа hashmap, собственного бинарного AVL-дерева и собственной хеш-таблицы, а также префиксного дерева "trie". 

   * Наша задача — сравнить быстродействие этих вариантов. Таким образом, для каждой структуры мы засекаем два параметра: время загрузки словаря и чистое время на проверку всех файлов (время на чтение-запись, вывод в консоль и т.д. не учитывается!)

   * Внутри рекомендуется использовать полиморфизм — создать базовый класс сhecker, с методами add и check (можно добавлять сколько угодно дополнительных, если нужно). От этого класса унаследовать классы вида hashChecker, binTreeChecker и т.д., и дальше организовать код максимально выдержав принцип DRY

На вход программа ничего не принимает — считаем, что словарь лежит в папке проекта и называется dictionary.txt, а тексты лежат в папке texts.

На выход программа выдаёт в консоль через пробел: название структуры данных (без пробелов), время загрузки словаря, суммарное время проверки всех текстов, суммарное количество проверенных слов и суммарное количество «неправильных» слов. Время везде в миллисекундах. Каждая структура — вывод с новой строки.

**Пример:** \
bintree 790 189709 950123 34295 \
hashtable 1280 50570 950123 34295 \
stdvector 340 970223 950123 34295 

**Обратите внимание — общее количество слов и количество ошибок одинаково для всех структур данных — если это не так, то где-то ошибка**