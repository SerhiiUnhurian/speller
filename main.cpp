#include <iostream>
#include <fstream>
#include <filesystem>
#include <memory>
#include <stdexcept>

#include "src/VectorDictionary.h"
#include "src/BinaryDictionary.h"
#include "src/SetDictionary.h"
#include "src/TrieDictionary.h"
#include "src/HashTableDictionary.h"

#define DICTIONARY "../dictionary.txt"
#define TEXTS "../texts/"

#define MAX_WORD_LENGTH 45

struct Speller {
    std::string name;
    uint32_t misspellings {};
    uint32_t checkedWords {};
    std::chrono::milliseconds loadTime {};
    std::chrono::milliseconds checkTime {};
//    std::chrono::steady_clock::duration loadTime {};
//    std::chrono::steady_clock::duration checkTime {};
};

void speller(Dictionary &dictionary) {
    // Prepare to spell-check
    namespace fs = std::filesystem;
    Speller speller;
    std::string word;
    speller.name = dictionary.getName();

    auto loadStart = std::chrono::steady_clock::now();
    // Load dictionary
    bool loaded = dictionary.load(DICTIONARY);
    auto loadEnd  = std::chrono::steady_clock::now();
    speller.loadTime = std::chrono::duration_cast<std::chrono::milliseconds>(loadEnd - loadStart);

    // Exit if dictionary not loaded
    if (!loaded) {
        throw std::runtime_error("Could not load dictionary!\n");
    }

    // Creates directory for check reports.
    const fs::path textsDir { TEXTS };
    const fs::path checksDir {fs::current_path().parent_path() / "check-reports"};
    fs::create_directory(checksDir);

    // Iterate through every file in directory with texts.
    for (const auto &text: fs::directory_iterator(textsDir)) {
        std::ifstream inFile(text.path());
        std::ofstream outFile(checksDir / text.path().filename());

        if (!inFile || !outFile) {
           throw std::runtime_error("Could not input or output file!\n");
        }

        // Spell-check each word in text
        for (int ch = inFile.get(); ch != EOF; ch = inFile.get()) {
            // Allow only alphabetical characters and apostrophes
            if (isalpha(ch) || (ch == '\'' && word.length() > 0)) {
                word += static_cast<char>(ch);

                if (word.length() > MAX_WORD_LENGTH) {
                    while ((ch = inFile.get()) != EOF && isalpha(ch));
                    word.clear();
                }
            } else if (isdigit(ch)) {
                while ((ch = inFile.get()) != EOF && isalnum(ch));
                word.clear();
            } else if (word.length() > 0) {
                speller.checkedWords++;

                auto checkStart = std::chrono::steady_clock::now();
                bool misspelled = !dictionary.check(word);
                auto checkEnd = std::chrono::steady_clock::now();
                speller.checkTime += std::chrono::duration_cast<std::chrono::milliseconds>(checkEnd - checkStart);

                if (misspelled) {
                    outFile << word << '\n';
                    speller.misspellings++;
                }
                word.clear();
            }
        }
        // Check for error while reading
        if (!inFile && !inFile.eof()) {
            inFile.close();
            throw std::runtime_error("Error reading text.\n");
        }

        inFile.close();
        outFile.close();
    }

    std::cout << speller.name << " " << /*std::chrono::duration_cast<std::chrono::milliseconds>*/(speller.loadTime).count()
            << " " << /*std::chrono::duration_cast<std::chrono::milliseconds>*/(speller.checkTime).count()
            << " " << speller.checkedWords << " " << speller.misspellings << std::endl;
}

int main() {
    // Create dictionaries
    std::array<std::unique_ptr<Dictionary>, 5> dictionaries{
        std::make_unique<VectorDictionary>(),
        std::make_unique<BinaryDictionary>(),
        std::make_unique<SetDictionary>(),
        std::make_unique<TrieDictionary>(),
        std::make_unique<HashTableDictionary>(),
    };

    // Run speller check using every dictionary
    for (auto &dic : dictionaries) {
        try {
            speller(*dic);
        } catch(std::string& ex) {
            std::cout << ex;
            return 1;
        }
    }





    // Load dictionary
//    bool loaded = dictionary->load(DICTIONARY);

//    // Exit if dictionary not loaded
//    if (!loaded) {
//        std::cout << "Could not load dictionary!\n";
//        return 1;
//    }
//
//    // Prepare input and output files
//    std::ifstream inFile("../texts/dracula.txt");
//    std::ofstream outFile("../check-reports/dracula.txt");
//    if (!inFile || !outFile) {
//        std::cout << "Could not input or output file!\n";
//        return 1;
//    }
//
//    // Prepare to spell-check
//    int misspellings = 0, words = 0;
//    std::string word;
//
//    // Spell-check each word in text
//    for (int ch = inFile.get(); ch != EOF; ch = inFile.get()) {
//        // Allow only alphabetical characters and apostrophes
//        if (isalpha(ch) || (ch == '\'' && word.length() > 0)) {
//            word += ch;
//
//            if (word.length() > LENGTH) {
//                while ((ch = inFile.get()) != EOF && isalpha(ch));
//                word.clear();
//            }
//        } else if (isdigit(ch)) {
//            while ((ch = inFile.get()) != EOF && isalnum(ch));
//            word.clear();
//        } else if (word.length() > 0) {
//            words++;
//
//            bool misspelled = !dictionary->check(word);
//            if (misspelled) {
//                outFile << word << '\n';
//                misspellings++;
//            }
//            word.clear();
//        }
//    }
//    // Check for error while reading
//    if (!inFile && !inFile.eof()) {
//        std::cout << "Error reading text.\n";
//        inFile.close();
//        return  1;
//    }
//
//    inFile.close();
//
//    std::cout << dictionary->getName() << " " << words << " " << misspellings << std::endl;

    return 0;
}











